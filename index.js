const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();

// app.use(express.json());
app.use(cors());
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: false }));

const db = require("./models");

// Routers
const userRoutes = require("./routes/Users");
const allergyRoutes = require("./routes/Allergies");
const behaviorRoutes = require("./routes/Behaviors");
const dietaryRoutes = require("./routes/Dietaries");
const diagnosisRoutes = require("./routes/Diagnosis");
const historyLogsRoutes = require("./routes/HistoryLogs");
const incidentReportsRoutes = require("./routes/IncidentReports");
const vitalsRoutes = require("./routes/Vitals");
const residentsRoutes = require("./routes/Residents");
const contactRoutes = require("./routes/ContactPerson");
const contactPhoneRoutes = require("./routes/ContactPersonPhone");
const contactEmailRoutes = require("./routes/ContactPersonEmail");
const contactAddressRoutes = require("./routes/ContactPersonAddress");
const communityRoutes = require("./routes/Communities");
const shiftSummaryLogRoutes = require("./routes/ShiftSummaryLog");
const historyRoutes = require("./routes/History");
const dailyLogRoutes = require("./routes/DailyLogs");
const woundRoutes = require("./routes/Wounds");
const emarRoutes = require("./routes/Emar");
const progressNoteRoutes = require("./routes/ProgressNotes");
const orderRoutes = require("./routes/Orders");
const assessmentsRoutes = require("./routes/Assessments");
const assessmentDataRoutes = require("./routes/AssessmentDataGeneral");
const assessmentDiagnosesAndAllergyRoutes = require("./routes/AssessmentDataDiagnosisAndAllergy");
const notificationRoutes = require("./routes/Notifications");
const bpRoutes = require("./routes/BloodPressure");
const bodyTempRoutes = require("./routes/BodyTemperature");
const pulseRoutes = require("./routes/PulseRate");
const applicationRoutes = require("./routes/Applications");
const productRoutes = require("./routes/Products");
const careplanRoutes = require("./routes/CarePlan");
const blogRoutes = require("./routes/Blog");
const routinesRoutes = require("./routes/Routines");
const insuranceInfoRoutes = require("./routes/InsuranceInfo");
const disabilitiesRoutes = require("./routes/Disabilities");
const HealthCareDirectivesRoutes = require("./routes/HealthCareDirectives");

// lookup
const ResponsibilityGuardianLookupRoutes = require("./routes/ResponsibilityGuardianLookup");
const CodeStatusLookupRoutes = require("./routes/CodeStatusLookup");
const RoomPrefLookupRoutes = require("./routes/RoomPrefLookup");
const AllergiesLookupRoutes = require("./routes/AllergiesLookup");
const DiabetesControlLookup = require("./routes/DiabetesControlLookup");
const OxygenUsedLookupRoutes = require("./routes/OxygenUsedLookup");
const OxygenEnablingDeviceLookupRoutes = require("./routes/OxygenEnablingDeviceLookup");
const MedicationLevelofAssistanceLookupRoutes = require("./routes/MedicationLevelAssistanceLookup");
const PrefferedPharmacyUtilizationLookupRoutes = require("./routes/PrefferedPharmacyUtilizationLookup");
// new
const BehaviorHistoryLookupRoutes = require("./routes/BehaviorHistoryLookup");
const OrientationLookupRoutes = require("./routes/OrientationLookup");
const ShortTermMemoryLookupRoutes = require("./routes/ShortTermMemoryLookup");
const LongTermMemoryLookupRoutes = require("./routes/LongTermMemoryLookup");
const VisionLookupRoutes = require("./routes/VisionLookup");
const CommunicationLookupRoutes = require("./routes/CommunicationLookup");
const CommunuicableDeviceAndMethodLookupRoutes = require("./routes/CommunuicableDeviceAndMethodLookup");
const MobilityAmbulationLevelofAssistanceLookupRoutes = require("./routes/MobilityAmbulationLevelofAssistanceLookup");
const MobilityAmbulationEnablingDeviceAndMethodLookupRoutes = require("./routes/MobilityAmbulationEnablingDeviceAndMethodLookup");
const ParalysisLookupRoutes = require("./routes/ParalysisLookup");
const TransferringEnablingDeviceandMethodLookupRoutes = require("./routes/TransferringEnablingDeviceandMethodLookup");
const ResidentIsAbleToLookupRoutes = require("./routes/ResidentIsAbleToLookup");
const EliminationContinenceLookupRoutes = require("./routes/EliminationContinenceLookup");
const BathingLevelofAssistanceLookupRoutes = require("./routes/BathingLevelofAssistanceLookup");
const BathingFrequencyLookupRoutes = require("./routes/BathingFrequencyLookup");
const BathingPreferencesLookupRoutes = require("./routes/BathingPreferencesLookup");
const BathingEnablingDeviceandMethodLookupRoutes = require("./routes/BathingEnablingDeviceandMethodLookup");
const GroomingPersonalHygieneLevelofAssistanceLookupRoutes = require("./routes/GroomingPersonalHygieneLevelofAssistanceLookup");
const DressingLevelofAssistanceLookupRoutes = require("./routes/DressingLevelofAssistanceLookup");
const ToiletingLevelofAssistanceLookupRoutes = require("./routes/ToiletingLevelofAssistanceLookup");
const ToiletingEnablingDevicesandMethodsLookupRoutes = require("./routes/ToiletingEnablingDevicesandMethodsLookup");
const MealConsumptionofAssistanceLookupRoutes = require("./routes/MealConsumptionofAssistanceLookup");
const DietNeedsLookupRoutes = require("./routes/DietNeedsLookup");
const AdditionalMealConsiderationLookupRoutes = require("./routes/AdditionalMealConsiderationLookup");
const AdditionalNursingServiceOutsideServiceLookupRoutes = require("./routes/AdditionalNursingServiceOutsideServiceLookup");
const ResidentConditionLookupRoutes = require("./routes/ResidentConditionLookup");
const TransferringLevelofAssistanceLookupRoutes = require("./routes/TransferringLevelofAssistanceLookup");
const GrowingPersonalHygieneEnablingDevicesandMethodLookupRoutes = require("./routes/GrowingPersonalHygieneEnablingDevicesandMethodLookup");
const BasicInfoRoutes = require("./routes/BasicInfo");
const EDocumentsRoutes = require("./routes/EDocuments");

// const { Sequelize, sequelize } = require("./models");

app.use("/users", userRoutes);
app.use("/allergy", allergyRoutes);
app.use("/behavior", behaviorRoutes);
app.use("/dietary", dietaryRoutes);
app.use("/diagnosis", diagnosisRoutes);
app.use("/history", historyLogsRoutes);
app.use("/incident", incidentReportsRoutes);
app.use("/vitals", vitalsRoutes);
app.use("/residents", residentsRoutes);
app.use("/contact", contactRoutes);
app.use("/contactPhone", contactPhoneRoutes);
app.use("/contactEmail", contactEmailRoutes);
app.use("/contactAddress", contactAddressRoutes);
app.use("/community", communityRoutes);
app.use("/shiftsummarylog", shiftSummaryLogRoutes);
app.use("/history", historyRoutes);
app.use("/dailylog", dailyLogRoutes);
app.use("/wound", woundRoutes);
app.use("/emar", emarRoutes);
app.use("/note", progressNoteRoutes);
app.use("/order", orderRoutes);
app.use("/assessments", assessmentsRoutes);
app.use("/assessmentData", assessmentDataRoutes);
app.use("/assessmentDiagnosesAndAllergy", assessmentDiagnosesAndAllergyRoutes);
app.use("/notification", notificationRoutes);
app.use("/bloodpressure", bpRoutes);
app.use("/bodytemp", bodyTempRoutes);
app.use("/pulserate", pulseRoutes);
app.use("/blog", blogRoutes);
app.use("/application", applicationRoutes);
app.use("/product", productRoutes);
app.use("/careplan", careplanRoutes);
app.use("/routine", routinesRoutes);
app.use("/insuranceinfo", insuranceInfoRoutes);
app.use("/disabilities", disabilitiesRoutes);
app.use("/edocuments", EDocumentsRoutes);

// lookup
app.use("/responsibilityGuardianLookup", ResponsibilityGuardianLookupRoutes);
app.use("/codeStatusLookup", CodeStatusLookupRoutes);
app.use("/roomPrefLookup", RoomPrefLookupRoutes);
app.use("/allergiesLookup", AllergiesLookupRoutes);
app.use("/diabetesLookup", DiabetesControlLookup);
app.use("/oxygenusedlookup", OxygenUsedLookupRoutes);
app.use("/oxygenEnablingDeviceLookup", OxygenEnablingDeviceLookupRoutes);
app.use(
  "/medicationLevelofAssistanceLookup",
  MedicationLevelofAssistanceLookupRoutes
);
app.use(
  "/prefferedPharmacyUtilizationLookup",
  PrefferedPharmacyUtilizationLookupRoutes
);

// new
app.use("/behaviorHistoryLookup", BehaviorHistoryLookupRoutes);
app.use("/orientationLookup", OrientationLookupRoutes);
app.use("/shortTermMemoryLookup", ShortTermMemoryLookupRoutes);
app.use("/longTermMemoryLookup", LongTermMemoryLookupRoutes);
app.use("/visionLookup", VisionLookupRoutes);
app.use("/communicationLookup", CommunicationLookupRoutes);
app.use(
  "/communuicableDeviceAndMethodLookup",
  CommunuicableDeviceAndMethodLookupRoutes
);
app.use(
  "/mobilityAmbulationLevelofAssistanceLookup",
  MobilityAmbulationLevelofAssistanceLookupRoutes
);
app.use(
  "/mobilityAmbulationEnablingDeviceAndMethodLookup",
  MobilityAmbulationEnablingDeviceAndMethodLookupRoutes
);
app.use("/paralysisLookup", ParalysisLookupRoutes);
app.use(
  "/transferringEnablingDeviceandMethodLookup",
  TransferringEnablingDeviceandMethodLookupRoutes
);
app.use("/residentIsAbleToLookup", ResidentIsAbleToLookupRoutes);
app.use("/eliminationContinenceLookup", EliminationContinenceLookupRoutes);
app.use(
  "/bathingLevelofAssistanceLookup",
  BathingLevelofAssistanceLookupRoutes
);
app.use("/bathingFrequencyLookup", BathingFrequencyLookupRoutes);
app.use("/bathingPreferencesLookup", BathingPreferencesLookupRoutes);
app.use(
  "/bathingEnablingDeviceandMethodLookup",
  BathingEnablingDeviceandMethodLookupRoutes
);
app.use(
  "/groomingPersonalHygieneLevelofAssistanceLookup",
  GroomingPersonalHygieneLevelofAssistanceLookupRoutes
);
app.use(
  "/dressingLevelofAssistanceLookup",
  DressingLevelofAssistanceLookupRoutes
);
app.use(
  "/toiletingLevelofAssistanceLookup",
  ToiletingLevelofAssistanceLookupRoutes
);
app.use(
  "/toiletingEnablingDevicesandMethodsLookup",
  ToiletingEnablingDevicesandMethodsLookupRoutes
);
app.use(
  "/mealConsumptionofAssistanceLookup",
  MealConsumptionofAssistanceLookupRoutes
);
app.use("/dietNeedsLookup", DietNeedsLookupRoutes);
app.use(
  "/additionalMealConsiderationLookup",
  AdditionalMealConsiderationLookupRoutes
);
app.use(
  "/additionalNursingServiceOutsideServiceLookup",
  AdditionalNursingServiceOutsideServiceLookupRoutes
);
app.use("/residentConditionLookup", ResidentConditionLookupRoutes);
app.use(
  "/transferringLevelofAssistanceLookup",
  TransferringLevelofAssistanceLookupRoutes
);
app.use(
  "/growingPersonalHygieneEnablingDevicesandMethodLookup",
  GrowingPersonalHygieneEnablingDevicesandMethodLookupRoutes
);
app.use("/basicInfo", BasicInfoRoutes);
app.use("/healthcareDirectives", HealthCareDirectivesRoutes);

const port = process.env.PORT || 4000;

// db.Sequelize = Sequelize;
// db.sequelize = sequelize;

db.sequelize.sync().then(() => {
  app.listen(port, () => {
    console.log(`server is running at port ${port}`);
  });
});
