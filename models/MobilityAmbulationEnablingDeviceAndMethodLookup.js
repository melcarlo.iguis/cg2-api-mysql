module.exports = (sequelize, DataTypes) => {
  const MobilityAmbulationEnablingDeviceAndMethodLookup = sequelize.define(
    "MobilityAmbulationEnablingDeviceAndMethodLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return MobilityAmbulationEnablingDeviceAndMethodLookup;
};
