module.exports = (sequelize, DataTypes) => {
  const Communities = sequelize.define("Communities", {
    communityName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    isOccupied: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  });

  return Communities;
};
