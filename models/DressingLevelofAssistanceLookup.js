module.exports = (sequelize, DataTypes) => {
  const DressingLevelofAssistanceLookup = sequelize.define(
    "DressingLevelofAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return DressingLevelofAssistanceLookup;
};
