module.exports = (sequelize, DataTypes) => {
  const GrowingPersonalHygieneEnablingDevicesandMethodLookup = sequelize.define(
    "GrowingPersonalHygieneEnablingDevicesandMethodLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return GrowingPersonalHygieneEnablingDevicesandMethodLookup;
};
