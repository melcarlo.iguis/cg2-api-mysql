module.exports = (sequelize, DataTypes) => {

    const DailyLogs = sequelize.define("DailyLogs" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
         
        residentName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        
        userName: {
            type: DataTypes.STRING,
            allowNull: false,
        },

    })

    return DailyLogs
}