module.exports = (sequelize, DataTypes) => {
  const Routines = sequelize.define("Routines", {
    name: {
      type: DataTypes.STRING,
    },
    type: {
      type: DataTypes.STRING,
    },

    frequency: {
      type: DataTypes.STRING,
    },
  });

  return Routines;
};
