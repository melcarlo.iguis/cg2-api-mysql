module.exports = (sequelize, DataTypes) => {
  const ContactPerson = sequelize.define("ContactPerson", {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    middleName: {
      type: DataTypes.STRING,
      allowNull: true,
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    relationship: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    responsibility: {
      type: DataTypes.JSON,
      allowNull: false,
    },
  });

  // define the association here
  ContactPerson.associate = (models) => {
    ContactPerson.hasMany(models.ContactPersonEmail, {
      onDelete: "cascade",
    });
    ContactPerson.hasMany(models.ContactPersonPhone, {
      onDelete: "cascade",
    });
    ContactPerson.hasMany(models.ContactPersonAddress, {
      onDelete: "cascade",
    });
  };

  return ContactPerson;
};
