module.exports = (sequelize, DataTypes) => {
  const BathingEnablingDeviceandMethodLookup = sequelize.define(
    "BathingEnablingDeviceandMethodLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return BathingEnablingDeviceandMethodLookup;
};
