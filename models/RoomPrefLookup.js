module.exports = (sequelize, DataTypes) => {
  const RoomPrefLookup = sequelize.define(
    "RoomPrefLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return RoomPrefLookup;
};
