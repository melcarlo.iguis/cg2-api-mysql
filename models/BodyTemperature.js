module.exports = (sequelize, DataTypes) => {

    const BodyTemperature = sequelize.define("BodyTemperature" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        bodyTemp: {
            type: DataTypes.STRING,
        },

    })

    return BodyTemperature
}