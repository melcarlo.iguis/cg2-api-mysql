module.exports = (sequelize, DataTypes) => {
  const BathingFrequencyLookup = sequelize.define(
    "BathingFrequencyLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return BathingFrequencyLookup;
};
