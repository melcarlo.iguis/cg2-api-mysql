module.exports = (sequelize, DataTypes) => {
  const AdditionalNursingServiceOutsideServiceLookup = sequelize.define(
    "AdditionalNursingServiceOutsideServiceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return AdditionalNursingServiceOutsideServiceLookup;
};
