module.exports = (sequelize, DataTypes) => {
  const LongTermMemoryLookup = sequelize.define(
    "LongTermMemoryLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return LongTermMemoryLookup;
};
