module.exports = (sequelize, DataTypes) => {
  const Products = sequelize.define(
    "Products",
    {
      ApplNo: {
        type: DataTypes.STRING,
      },

      ProductNo: {
        type: DataTypes.STRING,
      },

      Form: {
        type: DataTypes.STRING,
      },

      Strength: {
        type: DataTypes.STRING,
      },

      ReferenceDrug: {
        type: DataTypes.INTEGER,
      },

      DrugName: {
        type: DataTypes.STRING,
      },

      ActiveIngredient: {
        type: DataTypes.STRING,
      },

      ReferenceStandard: {
        type: DataTypes.INTEGER,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  Products.removeAttribute("id");
  return Products;
};
