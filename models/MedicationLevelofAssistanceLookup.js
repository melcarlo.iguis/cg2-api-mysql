module.exports = (sequelize, DataTypes) => {
  const MedicationLevelAssistanceLookup = sequelize.define(
    "MedicationLevelAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return MedicationLevelAssistanceLookup;
};
