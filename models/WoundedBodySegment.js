module.exports = (sequelize, DataTypes) => {
  const WoundedBodySegment = sequelize.define("WoundedBodySegment", {
    woundedBodySegment: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  WoundedBodySegment.associate = (models) => {
    WoundedBodySegment.hasMany(models.GeneralBodySegment, {
      onDelete: "cascade",
    });
  };

  return WoundedBodySegment;
};
