module.exports = (sequelize, DataTypes) => {
  const ResponsibilityGuardianLookup = sequelize.define(
    "ResponsibilityGuardianLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ResponsibilityGuardianLookup;
};
