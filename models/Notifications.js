module.exports = (sequelize, DataTypes) => {
  const Notifications = sequelize.define("Notifications", {
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
    },

    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    tenantName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    actionNeeded: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    priorityLevel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },

    isCompleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },

    dateAccomplish: {
      type: DataTypes.DATE,
    },

    value: {
      type: DataTypes.STRING,
    },

    carePlanID: {
      type: DataTypes.INTEGER,
    },
  });

  return Notifications;
};
