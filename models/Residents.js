module.exports = (sequelize, DataTypes) => {
  const Residents = sequelize.define("Residents", {
    picture: {
      type: DataTypes.STRING,
    },

    cloudinary_id: {
      type: DataTypes.STRING,
    },

    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    middleName: {
      type: DataTypes.STRING,
      allowNull: true,
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    birthday: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    status: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },

    careLevel: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    communityName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  // define the association here
  Residents.associate = (models) => {
    Residents.hasMany(models.Allergies, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Behaviors, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Diagnosis, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Dietaries, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.IncidentReports, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Vitals, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.ContactPerson, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.HistoryLogs, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Notifications, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.ShiftSummaryLog, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.History, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.DailyLogs, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Wounds, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Emar, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.ProgressNotes, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Orders, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Assessments, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.BloodPressure, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.BodyTemperature, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.PulseRate, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Respiration, {
      onDelete: "cascade",
    });

    Residents.hasMany(models.CarePlan, {
      onDelete: "cascade",
    });
    Residents.hasOne(models.BasicInfo, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.InsuranceInfo, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Routines, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.Disabilities, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.HealthCareDirectives, {
      onDelete: "cascade",
    });
    Residents.hasMany(models.EDocuments, {
      onDelete: "cascade",
    });
  };

  return Residents;
};
