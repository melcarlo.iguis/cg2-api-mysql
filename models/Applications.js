module.exports = (sequelize, DataTypes) => {
  const Applications = sequelize.define(
    "Applications",
    {
      ApplNo: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },

      ApplType: {
        type: DataTypes.STRING,
      },

      ApplPublicNotes: {
        type: DataTypes.STRING,
      },

      SponsorName: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return Applications;
};
