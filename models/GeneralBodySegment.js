module.exports = (sequelize, DataTypes) => {
  const GeneralBodySegment = sequelize.define("GeneralBodySegment", {
    generalBodySegment: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  GeneralBodySegment.associate = (models) => {
    GeneralBodySegment.hasMany(models.SpecificWoundLocation, {
      onDelete: "cascade",
    });
  };
  return GeneralBodySegment;
};
