module.exports = (sequelize, DataTypes) => {
  const SpecificWoundLocation = sequelize.define("SpecificWoundLocation", {
    specificWoundLocation: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return SpecificWoundLocation;
};
