module.exports = (sequelize, DataTypes) => {
  const CommunicationLookup = sequelize.define(
    "CommunicationLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return CommunicationLookup;
};
