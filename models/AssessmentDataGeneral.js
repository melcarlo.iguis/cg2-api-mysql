module.exports = (sequelize, DataTypes) => {
  const AssessmentDataGeneral = sequelize.define(
    "AssessmentDataGeneral",
    {
      facilityPreference: {
        type: DataTypes.JSON,
      },
      roomPreference: {
        type: DataTypes.JSON,
      },
      legalGuardian: {
        type: DataTypes.JSON,
      },
      codeStatus: {
        type: DataTypes.JSON,
      },
      primaryPhysician: {
        type: DataTypes.STRING,
      },
      primaryPhysicianTelNo: {
        type: DataTypes.STRING,
      },
      primaryPhysicianAddress: {
        type: DataTypes.STRING,
      },
      mortuaryInformation: {
        type: DataTypes.STRING,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return AssessmentDataGeneral;
};
