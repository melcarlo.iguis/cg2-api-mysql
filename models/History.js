module.exports = (sequelize, DataTypes) => {

    const History = sequelize.define("History" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
         
        tenantName: {
            type: DataTypes.STRING,
            allowNull: false,
        },

    })

    return History
}