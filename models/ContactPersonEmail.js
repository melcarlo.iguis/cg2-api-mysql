module.exports = (sequelize, DataTypes) => {
  const ContactPersonEmail = sequelize.define("ContactPersonEmail", {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return ContactPersonEmail;
};
