module.exports = (sequelize, DataTypes) => {
  const CommunicableDiseasesLookup = sequelize.define(
    "CommunicableDiseasesLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return CommunicableDiseasesLookup;
};
