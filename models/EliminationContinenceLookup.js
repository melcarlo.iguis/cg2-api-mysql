module.exports = (sequelize, DataTypes) => {
  const EliminationContinenceLookup = sequelize.define(
    "EliminationContinenceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return EliminationContinenceLookup;
};
