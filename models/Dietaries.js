module.exports = (sequelize, DataTypes) => {
  const Dietaries = sequelize.define("Dietaries", {
    dietaryPreference: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    startDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    dcDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return Dietaries;
};
