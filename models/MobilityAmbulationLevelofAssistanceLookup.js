module.exports = (sequelize, DataTypes) => {
  const MobilityAmbulationLevelofAssistanceLookup = sequelize.define(
    "MobilityAmbulationLevelofAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return MobilityAmbulationLevelofAssistanceLookup;
};
