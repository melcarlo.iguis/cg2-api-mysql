const Users = require("./Users");

module.exports = (sequelize, DataTypes) => {
  const Allergies = sequelize.define("Allergies", {
    allergy: {
      type: DataTypes.STRING,
    },

    allergyType: {
      type: DataTypes.STRING,
    },

    allergenType: {
      type: DataTypes.STRING,
    },

    startDate: {
      type: DataTypes.DATE,
    },

    endDate: {
      type: DataTypes.DATE,
    },

    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  // Allergies.associate = (models) => {
  //   Allergies.belongsTo(models.Users);
  // };

  return Allergies;
};
