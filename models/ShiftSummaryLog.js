module.exports = (sequelize, DataTypes) => {
  const ShiftSummaryLog = sequelize.define("ShiftSummaryLog", {
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
    },

    activity: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    residentName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    userName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return ShiftSummaryLog;
};
