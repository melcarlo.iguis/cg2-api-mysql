module.exports = (sequelize, DataTypes) => {
  const HealthCareDirectives = sequelize.define("HealthCareDirectives", {
    codeStatus: {
      type: DataTypes.JSON,
    },
    hasAdvanceOrder: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    advanceCareOrder: {
      type: DataTypes.JSON,
    },
    advanceCareOrderPicture: {
      type: DataTypes.STRING,
    },
    notes: {
      type: DataTypes.STRING,
    },
    // personActingforResident: {
    //   type: DataTypes.STRING,
    // },
    primaryPhysician: {
      type: DataTypes.STRING,
    },
    primaryPhysicianTelNo: {
      type: DataTypes.STRING,
    },
    primaryPhysicianAddress: {
      type: DataTypes.STRING,
    },
    MortuaryInfo: {
      type: DataTypes.STRING,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return HealthCareDirectives;
};
