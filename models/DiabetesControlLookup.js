module.exports = (sequelize, DataTypes) => {
  const DiabetesControlLookup = sequelize.define(
    "DiabetesControlLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return DiabetesControlLookup;
};
