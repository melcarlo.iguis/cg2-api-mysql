module.exports = (sequelize, DataTypes) => {
  const ContactPersonPhone = sequelize.define("ContactPersonPhone", {
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return ContactPersonPhone;
};
