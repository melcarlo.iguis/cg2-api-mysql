module.exports = (sequelize, DataTypes) => {
  const Behaviors = sequelize.define("Behaviors", {
    behavior: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    startDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    location: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    alterable: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    riskTo: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    times: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    triggerBy: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    review: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return Behaviors;
};
