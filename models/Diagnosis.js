module.exports = (sequelize, DataTypes) => {
  const Diagnosis = sequelize.define("Diagnosis", {
    diagnosisName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    startDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    endDate: {
      type: DataTypes.DATE,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return Diagnosis;
};
