module.exports = (sequelize, DataTypes) => {
  const OxygenEnablingDeviceLookup = sequelize.define(
    "OxygenEnablingDeviceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return OxygenEnablingDeviceLookup;
};
