module.exports = (sequelize, DataTypes) => {
  const AdditionalMealConsiderationLookup = sequelize.define(
    "AdditionalMealConsiderationLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return AdditionalMealConsiderationLookup;
};
