module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define("Users", {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    userType: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "staff",
    },

    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    middleName: {
      type: DataTypes.STRING,
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  Users.associate = (models) => {
    Users.hasMany(models.ShiftSummaryLog, {
      onDelete: "cascade",
    });
    // Users.hasMany(models.Allergies, {
    //   onDelete: "cascade",
    // });
  };

  return Users;
};
