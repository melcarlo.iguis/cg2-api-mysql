module.exports = (sequelize, DataTypes) => {
  const EDocuments = sequelize.define("EDocuments", {
    files: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    category: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return EDocuments;
};
