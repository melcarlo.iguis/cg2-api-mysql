module.exports = (sequelize, DataTypes) => {
  const OrientationLookup = sequelize.define(
    "OrientationLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return OrientationLookup;
};
