module.exports = (sequelize, DataTypes) => {

    const BloodPressure = sequelize.define("BloodPressure" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        systolic: {
            type: DataTypes.STRING,
        },

        diastolic: {
            type: DataTypes.STRING,
        },
        
    })

    return BloodPressure
}