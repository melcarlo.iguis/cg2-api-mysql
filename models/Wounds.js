module.exports = (sequelize, DataTypes) => {
  const Wounds = sequelize.define("Wounds", {
    picture: {
      type: DataTypes.STRING,
    },

    cloudinary_id: {
      type: DataTypes.STRING,
    },

    woundedBodySegment: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    generalWoundLocation: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    specificWoundLocation: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    dateDiscover: {
      type: DataTypes.DATE,
      allowNull: false,
    },

    onSetDate: {
      type: DataTypes.DATE,
    },

    woundPresentOn: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },

    woundHealing: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },

    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },

    anatomicalDirection: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    redness: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return Wounds;
};
