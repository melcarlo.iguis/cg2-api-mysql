module.exports = (sequelize, DataTypes) => {
  const ResidentIsAbleToLookup = sequelize.define(
    "ResidentIsAbleToLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ResidentIsAbleToLookup;
};
