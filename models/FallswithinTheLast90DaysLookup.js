module.exports = (sequelize, DataTypes) => {
  const FallswithinTheLast90DaysLookup = sequelize.define(
    "FallswithinTheLast90DaysLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return FallswithinTheLast90DaysLookup;
};
