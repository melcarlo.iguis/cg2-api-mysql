module.exports = (sequelize, DataTypes) => {
  const Assessments = sequelize.define("Assessments", {
    AssessmentCode: {
      type: DataTypes.INTEGER(8).UNSIGNED.ZEROFILL,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },

    type: {
      type: DataTypes.STRING,
    },

    reason: {
      type: DataTypes.STRING,
    },

    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },

    reviewDate: {
      type: DataTypes.DATE,
    },

    dateCompleted: {
      type: DataTypes.DATE,
    },
  });

  Assessments.associate = (models) => {
    Assessments.hasMany(models.AssessmentDataGeneral, {
      onDelete: "cascade",
      foreignKey: "AssessmentCode",
    });
    Assessments.hasMany(models.AssessmentDataDiagnosisAndAllergy, {
      onDelete: "cascade",
      foreignKey: "AssessmentCode",
    });
  };

  return Assessments;
};
