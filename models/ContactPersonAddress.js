module.exports = (sequelize, DataTypes) => {
  const ContactPersonAddress = sequelize.define("ContactPersonAddress", {
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  return ContactPersonAddress;
};
