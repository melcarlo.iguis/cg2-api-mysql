module.exports = (sequelize, DataTypes) => {
  const GroomingPersonalHygieneLevelofAssistanceLookup = sequelize.define(
    "GroomingPersonalHygieneLevelofAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return GroomingPersonalHygieneLevelofAssistanceLookup;
};
