module.exports = (sequelize, DataTypes) => {
  const ParalysisLookup = sequelize.define(
    "ParalysisLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ParalysisLookup;
};
