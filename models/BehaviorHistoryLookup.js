module.exports = (sequelize, DataTypes) => {
  const BehaviorHistoryLookup = sequelize.define(
    "BehaviorHistoryLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return BehaviorHistoryLookup;
};
