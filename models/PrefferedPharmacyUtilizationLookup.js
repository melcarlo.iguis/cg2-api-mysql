module.exports = (sequelize, DataTypes) => {
  const PrefferedPharmacyUtilizationLookup = sequelize.define(
    "PrefferedPharmacyUtilizationLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return PrefferedPharmacyUtilizationLookup;
};
