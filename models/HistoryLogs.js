module.exports = (sequelize, DataTypes) => {

    const HistoryLogs = sequelize.define("HistoryLogs" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
         
        tenantName: {
            type: DataTypes.STRING,
            allowNull: false,
        },

    })

    return HistoryLogs
}