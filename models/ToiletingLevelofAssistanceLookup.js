module.exports = (sequelize, DataTypes) => {
  const ToiletingLevelofAssistanceLookup = sequelize.define(
    "ToiletingLevelofAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ToiletingLevelofAssistanceLookup;
};
