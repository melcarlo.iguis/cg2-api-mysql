module.exports = (sequelize, DataTypes) => {
  const BasicInfo = sequelize.define("BasicInfo", {
    preferredName: {
      type: DataTypes.STRING,
    },
    contractType: {
      type: DataTypes.STRING,
    },
    placeofBirth: {
      type: DataTypes.STRING,
    },
    maritalStatus: {
      type: DataTypes.STRING,
    },
    race: {
      type: DataTypes.STRING,
    },
    affinity: {
      type: DataTypes.STRING,
    },
    languages: {
      type: DataTypes.JSON,
    },
    eyeColor: {
      type: DataTypes.STRING,
    },
    hairColor: {
      type: DataTypes.STRING,
    },
    identifyingMarks: {
      type: DataTypes.STRING,
    },
    churchNameorAffiliation: {
      type: DataTypes.STRING,
    },
    idPicture: {
      type: DataTypes.STRING,
    },
    cloudinary_id: {
      type: DataTypes.STRING,
    },
  });

  return BasicInfo;
};
