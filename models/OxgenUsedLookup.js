module.exports = (sequelize, DataTypes) => {
  const OxygenUsedLookup = sequelize.define(
    "OxygenUsedLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return OxygenUsedLookup;
};
