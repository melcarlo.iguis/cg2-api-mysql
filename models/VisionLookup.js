module.exports = (sequelize, DataTypes) => {
  const VisionLookup = sequelize.define(
    "VisionLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return VisionLookup;
};
