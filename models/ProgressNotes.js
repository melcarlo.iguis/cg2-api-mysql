module.exports = (sequelize, DataTypes) => {
  const ProgressNotes = sequelize.define("ProgressNotes", {
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return ProgressNotes;
};
