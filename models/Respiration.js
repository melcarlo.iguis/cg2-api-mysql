module.exports = (sequelize, DataTypes) => {

    const Respiration = sequelize.define("Respiration" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        respirationRate: {
            type: DataTypes.STRING,
        },

    })

    return Respiration
}