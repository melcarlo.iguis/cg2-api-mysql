module.exports = (sequelize, DataTypes) => {

    const Vitals = sequelize.define("Vitals" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        bodyTemp: {
            type: DataTypes.STRING,
        },

        pulseRate: {
            type: DataTypes.STRING,
        },

        respirationRate: {
            type: DataTypes.STRING,
        },

        systolic: {
            type: DataTypes.STRING,
        },

        diastolic: {
            type: DataTypes.STRING,
        },
        
    })

    return Vitals
}