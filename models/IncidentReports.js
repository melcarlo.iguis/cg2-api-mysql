module.exports = (sequelize, DataTypes) => {
  const IncidentReports = sequelize.define("IncidentReports", {
    date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
    },

    tenantStatement: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    incidentClass: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    location: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    incidentDescription: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    witness: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    firstAidGiven: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    firstAidNote: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    isVitalTaken: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    questionPainLvl: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    requestPhysician: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return IncidentReports;
};
