module.exports = (sequelize, DataTypes) => {
  const CodeStatusLookup = sequelize.define(
    "CodeStatusLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return CodeStatusLookup;
};
