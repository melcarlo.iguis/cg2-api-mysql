module.exports = (sequelize, DataTypes) => {
  const CarePlan = sequelize.define("CarePlan", {
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },

    medLevel: {
      type: DataTypes.STRING,
    },

    prefPharmacyUtilization: {
      type: DataTypes.JSON,
    },

    medName: {
      type: DataTypes.STRING,
    },

    medStrength: {
      type: DataTypes.STRING,
    },

    medForm: {
      type: DataTypes.STRING,
    },

    activeIngredient: {
      type: DataTypes.STRING,
    },

    priorityLevel: {
      type: DataTypes.INTEGER,
      defaultValue: 5,
    },
  });

  return CarePlan;
};
