module.exports = (sequelize, DataTypes) => {
  const AllergiesLookup = sequelize.define(
    "AllergiesLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return AllergiesLookup;
};
