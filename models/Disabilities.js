module.exports = (sequelize, DataTypes) => {
  const Disabilities = sequelize.define("Disabilities", {
    name: {
      type: DataTypes.STRING,
    },
    type: {
      type: DataTypes.STRING,
    },
    trigger: {
      type: DataTypes.STRING,
    },
    startDate: {
      type: DataTypes.DATE,
    },
  });

  return Disabilities;
};
