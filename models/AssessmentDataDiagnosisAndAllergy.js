module.exports = (sequelize, DataTypes) => {
  const AssessmentDataDiagnosisAndAllergy = sequelize.define(
    "AssessmentDataDiagnosisAndAllergy",
    {
      diagnosis: {
        type: DataTypes.JSON,
      },
      diagnosisNote: {
        type: DataTypes.STRING,
      },
      allergies: {
        type: DataTypes.JSON,
      },
      allergiesNote: {
        type: DataTypes.JSON,
      },
      reasonForHospitalization: {
        type: DataTypes.STRING,
      },
      reasonForAdmissionToRCPE: {
        type: DataTypes.STRING,
      },
      isSmoking: {
        type: DataTypes.BOOLEAN,
      },
      onOxygen: {
        type: DataTypes.BOOLEAN,
      },
      oxygenNote: {
        type: DataTypes.STRING,
      },
      IPPB: {
        type: DataTypes.BOOLEAN,
      },
      IPPBNote: {
        type: DataTypes.STRING,
      },
      colostomy: {
        type: DataTypes.BOOLEAN,
      },
      colostomyNote: {
        type: DataTypes.STRING,
      },
      enema: {
        type: DataTypes.BOOLEAN,
      },
      enemaNote: {
        type: DataTypes.STRING,
      },
      catheter: {
        type: DataTypes.BOOLEAN,
      },
      catheterNote: {
        type: DataTypes.STRING,
      },
      bowelorBladderIncontinence: {
        type: DataTypes.BOOLEAN,
      },
      bowelorBladderIncontinenceNote: {
        type: DataTypes.STRING,
      },
      contractures: {
        type: DataTypes.BOOLEAN,
      },
      contracturesNote: {
        type: DataTypes.STRING,
      },
      diabetes: {
        type: DataTypes.BOOLEAN,
      },
      diabetesNote: {
        type: DataTypes.STRING,
      },
      howDiabetesIsControlled: {
        type: DataTypes.JSON,
      },
      dialysis: {
        type: DataTypes.BOOLEAN,
      },
      dialysisNote: {
        type: DataTypes.STRING,
      },
      injection: {
        type: DataTypes.BOOLEAN,
      },
      injectionNote: {
        type: DataTypes.STRING,
      },
      woundBedSore: {
        type: DataTypes.BOOLEAN,
      },
      woundBedSoreNote: {
        type: DataTypes.STRING,
      },
      bedridden: {
        type: DataTypes.BOOLEAN,
      },
      berdiddenNote: {
        type: DataTypes.STRING,
      },
      gTubeorFeedingTube: {
        type: DataTypes.BOOLEAN,
      },
      gTubeorFeedingTubeNote: {
        type: DataTypes.STRING,
      },
      NGTube: {
        type: DataTypes.BOOLEAN,
      },
      NGTubeNote: {
        type: DataTypes.STRING,
      },
      infection: {
        type: DataTypes.BOOLEAN,
      },
      infectionNote: {
        type: DataTypes.STRING,
      },
      paralysis: {
        type: DataTypes.BOOLEAN,
      },
      paralysisNote: {
        type: DataTypes.STRING,
      },
      historyOfFall: {
        type: DataTypes.BOOLEAN,
      },
      historyOfFallNote: {
        type: DataTypes.STRING,
      },
      MCI: {
        type: DataTypes.BOOLEAN,
      },
      MCINote: {
        type: DataTypes.STRING,
      },
      communicableDiseases: {
        type: DataTypes.JSON,
      },
      oxygenUse: {
        type: DataTypes.JSON,
      },
      oxygenMethods: {
        type: DataTypes.JSON,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return AssessmentDataDiagnosisAndAllergy;
};
