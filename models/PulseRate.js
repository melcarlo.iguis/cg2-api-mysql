module.exports = (sequelize, DataTypes) => {

    const PulseRate = sequelize.define("PulseRate" , {
        
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
        },

        pulseRate: {
            type: DataTypes.STRING,
        },

    })

    return PulseRate
}