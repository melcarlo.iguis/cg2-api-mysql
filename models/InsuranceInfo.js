module.exports = (sequelize, DataTypes) => {
  const InsuranceInfo = sequelize.define("InsuranceInfo", {
    file: {
      type: DataTypes.STRING,
    },
    cloudinary_id: {
      type: DataTypes.STRING,
    },
    InsuranceName: {
      type: DataTypes.STRING,
    },
    PolicyNo: {
      type: DataTypes.STRING,
    },
    GroupNo: {
      type: DataTypes.STRING,
    },
    Telephone: {
      type: DataTypes.STRING,
    },
    ExpirationDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    creator: {
      type: DataTypes.STRING,
    },

    editor: {
      type: DataTypes.STRING,
    },
  });

  return InsuranceInfo;
};
