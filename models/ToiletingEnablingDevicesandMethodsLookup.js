module.exports = (sequelize, DataTypes) => {
  const ToiletingEnablingDevicesandMethodsLookup = sequelize.define(
    "ToiletingEnablingDevicesandMethodsLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ToiletingEnablingDevicesandMethodsLookup;
};
