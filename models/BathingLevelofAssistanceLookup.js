module.exports = (sequelize, DataTypes) => {
  const BathingLevelofAssistanceLookup = sequelize.define(
    "BathingLevelofAssistanceLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return BathingLevelofAssistanceLookup;
};
