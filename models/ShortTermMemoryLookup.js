module.exports = (sequelize, DataTypes) => {
  const ShortTermMemoryLookup = sequelize.define(
    "ShortTermMemoryLookup",
    {
      label: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      order: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
      },
    },
    {
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );

  return ShortTermMemoryLookup;
};
