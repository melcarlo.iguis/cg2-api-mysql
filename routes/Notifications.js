const express = require("express");
const router = express.Router();
const { Notifications } = require("../models");
const auth = require("../auth");

// router for adding Notifications data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Notifications.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for editing notification data
router.put("/:notificationId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Notifications.update(
    {
      priorityLevel: data.priorityLevel,
      isCompleted: data.isCompleted,
      dateAccomplish: data.dateAccomplish,
      value: data.value,
    },
    {
      where: {
        id: req.params.notificationId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});
// routes for fetching Notifications data
router.get("/fetch", auth.verify, async (req, res) => {
  const notificationList = await Notifications.findAll({});
  res.json(notificationList);
});

router.get("/:residentId/fetchAll", auth.verify, async (req, res) => {
  const residentId = req.params.residentId;
  const notificationList = await Notifications.findAll({
    where: { ResidentId: residentId },
  });
  res.json(notificationList);
});

// routes for fetching specific notification data
router.get("/:notificationId/fetch", auth.verify, async (req, res) => {
  const notificationId = req.params.notificationId;
  const notificationList = await Notifications.findAll({
    where: { id: notificationId },
  });
  res.json(notificationList);
});

module.exports = router;
