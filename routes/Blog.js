const express = require('express');
const router = express.Router();
const {Blog} = require('../models');

// routes for adding behavior data
router.post('/add' , async(req,res)=>{
    const data = req.body
    const addData = await Blog.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})


// routes for editing behavior data
router.put('/:blogId/update' , async(req,res)=>{
    const data = req.body
    await Behaviors.update({
        body : data.body,
        title : data.title,

    },
    {
        where:{
            id: req.params.blogId
        }
    }).then(result=>{
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for deleting behavior data
router.put('/:blogId/delete' , async(req,res)=>{
    const deleteBlog = await Blog.destroy({
        where: { id: req.params.blogId },
    })

    if(deleteBlog){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})

// routes for fetching all allergy data of specific resident
router.get('/fetch' , async (req,res)=>{
    const blog = await Blog.findAll({})
    res.json(blog)
})


module.exports = router