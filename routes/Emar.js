const express = require('express');
const router = express.Router();
const {Emar} = require('../models');
const auth = require('../auth');

// routes for adding Emar data
router.post('/add' , auth.verify, async(req,res)=>{
    const data = req.body
    const addData = await Emar.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for deleting Emar data
router.put('/:emarId/delete' , async(req,res)=>{
    const deleteEmar = await Emar.destroy({
        where: { id: req.params.emarId },
    })
    if(deleteEmar){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})

// routes for fetching Emar data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const emarList = await Emar.findAll({where: {ResidentId : residentsId}})
    res.json(emarList)
})

module.exports = router