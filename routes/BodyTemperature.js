const express = require('express');
const router = express.Router();
const {BodyTemperature} = require('../models')
const auth = require('../auth')

// routes for adding Blood Pressure data 
router.post('/add' , auth.verify , async(req,res)=>{
    const data = req.body
    const addData = BodyTemperature.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })   
})

// routes for editing Blood Pressure data
router.put('/:bodyTempId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await BodyTemperature.update({
        date: data.date,
        bodyTemp: data.bodyTemp,
    },
    {
        where:{
            id: req.params.bodyTempId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for fetching incident report data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const bodyTemp = await BodyTemperature.findAll({where: {ResidentId : residentsId}})
    res.json(bodyTemp)
})


// routes for deleting incident report data
router.put('/:bodyTempId/delete' , async(req,res)=>{
    const deleteBodyTemp = await BodyTemperature.destroy({
        where: { id: req.params.bodyTempId },
    })

    if(deleteBodyTemp){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})
module.exports = router