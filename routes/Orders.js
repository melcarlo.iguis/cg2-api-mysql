const express = require('express');
const router = express.Router();
const {Orders} = require('../models');
const auth = require('../auth');

// routes for adding Orders data
router.post('/add' , auth.verify, async(req,res)=>{
    const data = req.body
    const addData = await Orders.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for deleting Orders data
router.put('/:orderId/delete' , async(req,res)=>{
    const deleteOrders = await Orders.destroy({
        where: { id: req.params.orderId },
    })
    if(deleteOrders){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})

// routes for fetching Orders data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const orderList = await Orders.findAll({where: {ResidentId : residentsId}})
    res.json(orderList)
})

module.exports = router