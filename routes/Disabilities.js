const express = require("express");
const router = express.Router();
const { Disabilities } = require("../models");
const auth = require("../auth");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Disabilities.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing the data
router.put("/:id/update", auth.verify, async (req, res) => {
  const id = req.params.id;
  const data = req.body;
  await Disabilities.update(
    {
      name: data.name,
      type: data.type,
      trigger: data.trigger,
      startDate: data.startDate,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching all the data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const data = await Disabilities.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(data);
});

// routes for deleting incident report data
router.put("/:id/delete", async (req, res) => {
  const deleteData = await Disabilities.destroy({
    where: { id: req.params.id },
  });

  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
