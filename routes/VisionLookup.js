const express = require("express");
const router = express.Router();
const { VisionLookup } = require("../models");
const auth = require("../auth");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = VisionLookup.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing the data
router.put("/:id/update", auth.verify, async (req, res) => {
  const id = req.params.id;
  const data = req.body;
  await VisionLookup.update(
    {
      label: data.label,
      order: data.order,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching all the data
router.get("/fetch", auth.verify, async (req, res) => {
  const allData = await VisionLookup.findAll({});
  res.json(allData);
});

// routes for deleting incident report data
router.put("/:id/delete", async (req, res) => {
  const deleteData = await VisionLookup.destroy({
    where: { id: req.params.id },
  });

  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
