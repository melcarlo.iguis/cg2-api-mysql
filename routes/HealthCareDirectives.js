const express = require("express");
const router = express.Router();
const { HealthCareDirectives } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");
// routes for adding new resident's data
router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "documents",
    });
    try {
      const data = req.body;

      let healthcareDirectivesData = {
        advanceCareOrderPicture: result.secure_url,
        codeStatus: data.codeStatus,
        advanceCareOrder: data.advanceCareOrder,
        notes: data.notes,
        // personActingforResident: data.personActingforResident,
        primaryPhysician: data.primaryPhysician,
        primaryPhysicianTelNo: data.primaryPhysicianTelNo,
        primaryPhysicianAddress: data.primaryPhysicianAddress,
        MortuaryInfo: data.MortuaryInfo,
        ResidentId: data.ResidentId,
        creator: data.creator,
      };

      const addData = await HealthCareDirectives.create(
        healthcareDirectivesData
      ).then((saved) => {
        res.json(saved);
      });
    } catch (error) {
      res.json(error);
    }
  } catch (error) {
    res.json(error);
  }
});

// // routes for fetching the residents data
// router.get("/:residentId/fetch", auth.verify, async (req, res) => {
//   const resident = await Residents.findByPk(req.params.residentId);
//   res.json(resident);
// });

// routes for fetching incident report data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const healthCareData = await HealthCareDirectives.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(healthCareData);
});

// routes for deleting incident report data
router.put("/:healthCareDirectiveID/delete", async (req, res) => {
  const deleteHealthCareData = await HealthCareDirectives.destroy({
    where: { id: req.params.healthCareDirectiveID },
  });

  if (deleteHealthCareData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for editing incident reports data
router.put("/:healthCareDirectiveID/update", auth.verify, async (req, res) => {
  const data = req.body;

  await HealthCareDirectives.update(
    {
      codeStatus: data.codeStatus,
      advanceCareOrder: data.advanceCareOrder,
      notes: data.notes,
      // personActingforResident: data.personActingforResident,
      primaryPhysician: data.primaryPhysician,
      primaryPhysicianTelNo: data.primaryPhysicianTelNo,
      primaryPhysicianAddress: data.primaryPhysicianAddress,
      MortuaryInfo: data.MortuaryInfo,
    },
    {
      where: {
        id: req.params.healthCareDirectiveID,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
