const express = require("express");
const router = express.Router();
const { Diagnosis } = require("../models");
const auth = require("../auth");

// routes for adding Diagnosis data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Diagnosis.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing diagnosis data
router.put("/:diagnosisId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Diagnosis.update(
    {
      diagnosisName: data.diagnosisName,
      startDate: data.startDate,
      endDate: data.endDate,
    },
    {
      where: {
        id: req.params.diagnosisId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching all allergy data of specific resident
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const diagnosis = await Diagnosis.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(diagnosis);
});

// routes for deleting behavior data
router.put("/:diagnosisId/delete", async (req, res) => {
  const deleteDiagnosis = await Diagnosis.destroy({
    where: { id: req.params.diagnosisId },
  });

  if (deleteDiagnosis) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

module.exports = router;
