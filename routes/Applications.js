const express = require("express");
const router = express.Router();
const { Applications } = require("../models");
const auth = require("../auth");

router.get("/fetch", async (req, res) => {
  const allData = await Applications.findAll({});
  res.json(allData);
});

module.exports = router;
