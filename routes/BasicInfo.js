const express = require("express");
const multer = require("multer");
const router = express.Router();
const { BasicInfo } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding data
router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "cg2",
    });
    try {
      const data = req.body;

      let basicInfoData = {
        idPicture: result.secure_url,
        cloudinary_id: result.public_id,
        preferredName: data.preferredName,
        contractType: data.contractType,
        placeofBirth: data.placeofBirth,
        maritalStatus: data.maritalStatus,
        race: data.race,
        affinity: data.affinity,
        languages: data.languages,
        eyeColor: data.eyeColor,
        hairColor: data.hairColor,
        churchNameorAffiliation: data.churchNameorAffiliation,
        ResidentId: data.ResidentId,
      };

      const addData = await BasicInfo.create(basicInfoData).then((saved) => {
        res.json(saved);
        console.log(saved);
      });
    } catch (error) {
      res.json(error);
      console.log(error);
    }
  } catch (error) {
    res.json(error);
    console.log(error);
  }
});

router.put(
  "/:residentID/updatewithimage",
  upload.single("image"),
  auth.verify,
  async (req, res) => {
    try {
      const result = await cloudinary.uploader.upload(req.file.path, {
        upload_preset: "cg2",
      });

      console.log(result);
      const data = req.body;
      // console.log(result);
      await BasicInfo.update(
        {
          idPicture: result.secure_url,
          cloudinary_id: result.public_id,
          preferredName: data.preferredName,
          contractType: data.contractType,
          placeofBirth: data.placeofBirth,
          maritalStatus: data.maritalStatus,
          race: data.race,
          affinity: data.affinity,
          languages: data.languages,
          eyeColor: data.eyeColor,
          hairColor: data.hairColor,
          churchNameorAffiliation: data.churchNameorAffiliation,
        },
        {
          where: {
            ResidentId: req.params.residentID,
          },
        }
      )
        .then((updatedData) => {
          console.log(updatedData);
          res.json(updatedData);
        })
        .catch((err) => {
          console.error(err);
        });
    } catch (error) {
      console.log("no image");
      res.json(error);
    }
  }
);

router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const basicInfoList = await BasicInfo.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(basicInfoList);
});

router.put(
  "/:residentID/update",
  upload.single("image"),
  auth.verify,
  async (req, res) => {
    const data = req.body;
    // console.log(result);
    await BasicInfo.update(
      {
        preferredName: data.preferredName,
        contractType: data.contractType,
        placeofBirth: data.placeofBirth,
        maritalStatus: data.maritalStatus,
        race: data.race,
        affinity: data.affinity,
        languages: data.languages,
        eyeColor: data.eyeColor,
        hairColor: data.hairColor,
        churchNameorAffiliation: data.churchNameorAffiliation,
      },
      {
        where: {
          ResidentId: req.params.residentID,
        },
      }
    )
      .then((updatedData) => {
        console.log(updatedData);
        res.json(updatedData);
      })
      .catch((err) => {
        console.error(err);
      });
  }
);

module.exports = router;
