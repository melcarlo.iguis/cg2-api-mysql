const express = require("express");
const router = express.Router();
const { Wounds } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding wound data
router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "cg2-wound",
    });
    try {
      const data = req.body;

      let woundData = {
        picture: result.secure_url,
        cloudinary_id: result.public_id,
        woundedBodySegment: data.woundedBodySegment,
        generalWoundLocation: data.generalWoundLocation,
        specificWoundLocation: data.specificWoundLocation,
        dateDiscover: data.dateDiscover,
        onSetDate: data.onSetDate,
        woundPresentOn: data.woundPresentOn,
        woundHealing: data.woundHealing,
        anatomicalDirection: data.anatomicalDirection,
        redness: data.redness,
        ResidentId: data.ResidentId,
      };

      const addData = await Wounds.create(woundData).then((saved) => {
        res.json(saved);
      });
    } catch (error) {
      res.json(error);
    }
  } catch (error) {
    res.json(error);
  }
});

// routes for editing vitals data
router.put("/:woundId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Wounds.update(
    {
      woundedBodySegment: data.woundedBodySegment,
      generalWoundLocation: data.generalWoundLocation,
      specificWoundLocation: data.specificWoundLocation,
      dateDiscover: data.dateDiscover,
      onSetDate: data.onSetDate,
      woundPresentOn: data.woundPresentOn,
      woundHealing: data.woundHealing,
      anatomicalDirection: data.anatomicalDirection,
      redness: data.redness,
    },
    {
      where: {
        id: req.params.woundId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching wounds data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const woundList = await Wounds.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(woundList);
});

// routes for deleting incident report data
router.put("/:woundId/delete", async (req, res) => {
  const deleteWound = await Wounds.destroy({
    where: { id: req.params.woundId },
  });

  if (deleteWound) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
