const express = require('express');
const router = express.Router()
const {Communities} = require('../models')
const auth = require('../auth')

// routes for adding community data
router.post('/add' , auth.verify , async (req, res)=>{
    const data = req.body
    const addData = Communities.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for fetching the list of all available community
router.get('/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const allCommunity = await Communities.findAll()
    res.json(allCommunity)
})

module.exports = router