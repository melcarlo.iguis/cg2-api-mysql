const express = require("express");
const router = express.Router();
const {
  Residents,
  Allergies,
  BasicInfo,
  ContactPerson,
  ContactPersonPhone,
  ContactPersonEmail,
  ContactPersonAddress,
  Routines,
  Disabilities,
  InsuranceInfo,
  HealthCareDirectives,
  Wounds,
  Assessments,
  CarePlan,
  ProgressNotes,
  EDocuments,
} = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");
// routes for adding new resident's data
router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "cg2",
    });
    try {
      const data = req.body;

      let residentData = {
        picture: result.secure_url,
        cloudinary_id: result.public_id,
        firstName: data.firstName,
        middleName: data.middleName,
        lastName: data.lastName,
        birthday: data.birthday,
        status: data.status,
        address: data.address,
        careLevel: data.careLevel,
        communityName: data.communityName,
      };

      const addData = await Residents.create(residentData).then((saved) => {
        res.json(saved);
      });
    } catch (error) {
      res.json(error);
    }
  } catch (error) {
    res.json(error);
  }
});

// routes for fetching all the list of the residents
router.get("/fetch", auth.verify, async (req, res) => {
  const allResidents = await Residents.findAll({});
  res.json(allResidents);
});

// routes to fetch resident basic info and Contact person
router.get("/:residentId/fetchAllData", auth.verify, async (req, res) => {
  const allData = await Residents.findOne({
    where: { id: req.params.residentId },
    include: [
      {
        model: ContactPerson,
        include: [
          {
            model: ContactPersonEmail,
          },
          {
            model: ContactPersonPhone,
          },
          {
            model: ContactPersonAddress,
          },
        ],
      },
      {
        model: BasicInfo,
      },
      {
        model: Routines,
      },
      {
        model: Disabilities,
      },
      {
        model: InsuranceInfo,
      },
      {
        model: HealthCareDirectives,
      },
      {
        model: Wounds,
      },
      {
        model: Assessments,
      },
      {
        model: CarePlan,
      },
      {
        model: ProgressNotes,
      },
      {
        model: EDocuments,
      },
      // {
      //   model: Allergies,
      // },
    ],
  }).catch((err) => {
    console.error(err);
  });

  res.json(allData);
});

// routes for fetching the residents data
router.get("/:residentId/fetch", auth.verify, async (req, res) => {
  const resident = await Residents.findByPk(req.params.residentId);
  res.json(resident);
});

// routes for updating the resident's status data
router.put("/:residentId/changeStatus", auth.verify, async (req, res) => {
  const data = req.body;
  await Residents.update(
    {
      status: data.status,
    },
    {
      where: {
        id: req.params.residentId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});
module.exports = router;
