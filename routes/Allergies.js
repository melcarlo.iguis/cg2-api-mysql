const express = require("express");
const router = express.Router();
const { Allergies } = require("../models");
const auth = require("../auth");
const Users = require("../models/Users");
const Residents = require("../models/Residents");

// routes for adding allergy details
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  await Allergies.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for editing allergy details
router.put("/:allergyId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Allergies.update(
    {
      allergy: data.allergy,
      allergyType: data.allergyType,
      allergenType: data.allergenType,
      startDate: data.startDate,
      endDate: data.endDate,
    },
    {
      where: {
        id: req.params.allergyId,
      },
    }
  )
    .then((result) => {
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for deleting allergy details
router.put("/:allergyId/delete", async (req, res) => {
  const deleteAllergyData = await Allergies.destroy({
    where: { id: req.params.allergyId },
  });

  if (deleteAllergyData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for fetching all allergy data of specific resident
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const allergies = await Allergies.findAll({
    where: { ResidentId: residentsId },
  }).catch((err) => {
    console.error(err);
  });

  res.json(allergies);
});

module.exports = router;
