const express = require("express");
const multer = require("multer");
const router = express.Router();
const { Assessments } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Assessments.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing  data
router.put("/:AssessmentCode/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Assessments.update(
    {
      reviewDate: data.reviewDate,
      type: data.type,
    },
    {
      where: {
        AssessmentCode: req.params.AssessmentCode,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for setting assessments as complete
router.put("/:AssessmentCode/complete", auth.verify, async (req, res) => {
  const data = req.body;
  await Assessments.update(
    {
      dateCompleted: data.dateCompleted,
      status: true,
    },
    {
      where: {
        AssessmentCode: req.params.AssessmentCode,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching all the assessments list the resident have
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const assessmentList = await Assessments.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(assessmentList);
});

router.get("/:AssessmentCode/fetchOne", auth.verify, async (req, res) => {
  const AssessmentCode = req.params.AssessmentCode;
  const assessmentData = await Assessments.findByPk(AssessmentCode);
  res.json(assessmentData);
});

// routes for deleting allergy details
router.put("/:AssessmentCode/delete", async (req, res) => {
  const deleteAssessment = await Assessments.destroy({
    where: { AssessmentCode: req.params.AssessmentCode },
  });

  if (deleteAssessment) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

module.exports = router;
