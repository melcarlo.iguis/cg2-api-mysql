const express = require("express");
const router = express.Router();
const { ContactPersonPhone } = require("../models");
const auth = require("../auth");

// routes for adding Contact Person data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;

  let contactPersonData = {
    phoneNumber: data.phoneNumber,
    ContactPersonId: data.ContactPersonId,
  };
  const addData = ContactPersonPhone.create(contactPersonData)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

router.put("/:contactPersonPhoneID/delete", async (req, res) => {
  const deleteContactPersonPhone = await ContactPersonPhone.destroy({
    where: { id: req.params.contactPersonPhoneID },
  });

  if (deleteContactPersonPhone) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for editing incident reports data
router.put("/:contactPersonPhoneID/update", auth.verify, async (req, res) => {
  const data = req.body;

  await ContactPersonPhone.update(
    {
      phoneNumber: data.phoneNumber,
    },
    {
      where: {
        id: req.params.contactPersonPhoneID,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
