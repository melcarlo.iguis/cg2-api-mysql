const express = require("express");
const router = express.Router();
const { ShiftSummaryLog } = require("../models");
const auth = require("../auth");
const { Sequelize, Op, QueryTypes } = require("sequelize");

// routes for adding ShiftSummaryLog log data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = ShiftSummaryLog.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for editing diagnosis data
router.put("/:shiftSummaryLogId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await ShiftSummaryLog.update(
    {
      activity: data.activity,
      tenantName: data.tenantName,
    },
    {
      where: {
        id: req.params.shiftSummaryLogId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching shift summary log data of specific resident
router.get("/:userId/fetch", auth.verify, async (req, res) => {
  const userId = req.params.userId;
  const shiftLog = await ShiftSummaryLog.findAll({ where: { UserId: userId } });
  res.json(shiftLog);
});

router.get("/fetch", auth.verify, async (req, res) => {
  const allShiftLog = await ShiftSummaryLog.findAll({});
  res.json(allShiftLog);
});

// router.get("/fetchAll", async (req, res) => {
//   const records = await model.sequelize("SELECT * FROM Applications", {
//     nest: true,
//     type: QueryTypes.SELECT,
//   });
//   console.log(records);
//   res.json(records);
// });

module.exports = router;
