const express = require('express');
const router = express.Router();
const {Respiration} = require('../models')
const auth = require('../auth')

// routes for adding Blood Pressure data 
router.post('/add' , auth.verify , async(req,res)=>{
    const data = req.body
    const addData = Respiration.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })   
})

// routes for editing Blood Pressure data
router.put('/:respirationId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await Respiration.update({
        date: data.date,
        respirationRate: data.respirationRate,
    },
    {
        where:{
            id: req.params.respirationId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for fetching incident report data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const respirationData = await Respiration.findAll({where: {ResidentId : residentsId}})
    res.json(respirationData)
})


// routes for deleting incident report data
router.put('/:respirationId/delete' , async(req,res)=>{
    const deleteRespiration = await Respiration.destroy({
        where: { id: req.params.respirationId },
    })

    if(deleteRespiration){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})
module.exports = router