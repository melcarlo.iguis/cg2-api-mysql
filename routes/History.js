const express = require('express');
const router = express.Router()
const {History} = require('../models')
const auth = require('../auth')

// routes for adding History log data
router.post('/add' , auth.verify, async (req, res)=>{
    const data = req.body 
    const addData = History.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for editing diagnosis data
router.put('/:historyId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await History.update({
        date: data.date,
        title : data.title,
    },
    {
        where:{
            id: req.params.historyId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})


// routes for fetching history log data of specific resident
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const history = await History.findAll({where: {ResidentId : residentsId}})
    res.json(history)
})

// routes for fetching all history logs
router.get('/fetch' , auth.verify, async (req,res)=>{
    const allHistory = await History.findAll({})
    res.json(allHistory)
})



module.exports = router