const express = require('express');
const router = express.Router()
const {DailyLogs} = require('../models')
const auth = require('../auth')

// routes for adding Daily log data
router.post('/add' , auth.verify, async (req, res)=>{
    const data = req.body 
    const addData = DailyLogs.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for editing Daily logs data
router.put('/:dailyLogsId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await DailyLogs.update({
        date: data.date,
        title : data.title,
    },
    {
        where:{
            id: req.params.dailyLogsId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for fetching Daily log data of specific resident
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const dailyLogs = await DailyLogs.findAll({where: {ResidentId : residentsId}})
    res.json(dailyLogs)
})



module.exports = router