const express = require("express");
const router = express.Router();
const { Products } = require("../models");
const auth = require("../auth");

// routes for adding Blood Pressure data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Products.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

router.get("/fetch", async (req, res) => {
  const allData = await Products.findAll({});
  res.json(allData);
});

module.exports = router;
