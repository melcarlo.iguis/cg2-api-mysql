const express = require("express");
const router = express.Router();
const { ContactPersonAddress } = require("../models");
const auth = require("../auth");

// routes for adding Contact Person data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;

  let contactPersonData = {
    address: data.address,
    ContactPersonId: data.ContactPersonId,
  };
  const addData = ContactPersonAddress.create(contactPersonData)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

router.put("/:contactPersonAddressID/delete", async (req, res) => {
  const deleteContactPersonAddress = await ContactPersonAddress.destroy({
    where: { id: req.params.contactPersonAddressID },
  });

  if (deleteContactPersonAddress) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for editing incident reports data
router.put("/:personContactAddressID/update", auth.verify, async (req, res) => {
  const data = req.body;

  await ContactPersonAddress.update(
    {
      address: data.address,
    },
    {
      where: {
        id: req.params.personContactAddressID,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
