const express = require("express");
const router = express.Router();
const { IncidentReports } = require("../models");
const auth = require("../auth");

// router for adding incident reports data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = IncidentReports.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for editing incident reports data
router.put("/:incidentId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await IncidentReports.update(
    {
      date: data.date,
      tenantStatement: data.tenantStatement,
      incidentClass: data.incidentClass,
      location: data.location,
      incidentDescription: data.incidentDescription,
      witness: data.witness,
      firstAidGiven: data.firstAidGiven,
      firstAidNote: data.firstAidNote,
      isVitalTaken: data.isVitalTaken,
      questionPainLvl: data.questionPainLvl,
      requestPhysician: data.requestPhysician,
    },
    {
      where: {
        id: req.params.incidentId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for deleting incident report data
router.put("/:incidentReportId/delete", async (req, res) => {
  const deleteIncidentReportData = await IncidentReports.destroy({
    where: { id: req.params.incidentReportId },
  });

  if (deleteIncidentReportData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for fetching incident report data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const incidentReports = await IncidentReports.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(incidentReports);
});
module.exports = router;
