const express = require("express");
const router = express.Router();
const { BloodPressure } = require("../models");
const auth = require("../auth");

// routes for adding Blood Pressure data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = BloodPressure.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing Blood Pressure data
router.put("/:bpId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await BloodPressure.update(
    {
      date: data.date,
      systolic: data.systolic,
      diastolic: data.diastolic,
    },
    {
      where: {
        id: req.params.bpId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching incident report data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const bp = await BloodPressure.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(bp);
});

// routes for deleting incident report data
router.put("/:bpId/delete", async (req, res) => {
  const deleteBp = await BloodPressure.destroy({
    where: { id: req.params.bpId },
  });

  if (deleteBp) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
