const express = require("express");
const multer = require("multer");
const router = express.Router();
const { AssessmentDataGeneral } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = AssessmentDataGeneral.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for fetching all the assessments list the resident have
router.get("/fetch", auth.verify, async (req, res) => {
  const assessmentList = await AssessmentDataGeneral.findAll({});
  res.json(assessmentList);
});

// routes for fetching specific general assessments
router.get("/:AssessmentCode/fetch", auth.verify, async (req, res) => {
  const AssessmentCode = req.params.AssessmentCode;
  const assessmentList = AssessmentDataGeneral.findAll({
    where: { AssessmentCode: AssessmentCode },
  });
  res.json(assessmentList);
});

module.exports = router;
