const express = require("express");
const router = express.Router();
const { ProgressNotes } = require("../models");
const auth = require("../auth");

// routes for adding ProgressNotes data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = await ProgressNotes.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing progress notes data
router.put("/:noteId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await ProgressNotes.update(
    {
      date: data.date,
      description: data.description,
    },
    {
      where: {
        id: req.params.noteId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for deleting ProgressNotes data
router.put("/:noteId/delete", async (req, res) => {
  const deleteProgressNotes = await ProgressNotes.destroy({
    where: { id: req.params.noteId },
  });
  if (deleteProgressNotes) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for fetching ProgressNotes data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const noteList = await ProgressNotes.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(noteList);
});

module.exports = router;
