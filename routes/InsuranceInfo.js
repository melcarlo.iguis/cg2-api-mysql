const express = require("express");
const router = express.Router();
const { InsuranceInfo } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding data
router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "cg2",
    });
    try {
      const data = req.body;

      let insuranceInfoData = {
        file: result.secure_url,
        cloudinary_id: result.public_id,
        InsuranceName: data.InsuranceName,
        PolicyNo: data.PolicyNo,
        GroupNo: data.GroupNo,
        Telephone: data.Telephone,
        ExpirationDate: data.ExpirationDate,
        ResidentId: data.ResidentId,
        creator: data.creator,
      };

      const addData = await InsuranceInfo.create(insuranceInfoData).then(
        (saved) => {
          res.json(saved);
        }
      );
    } catch (error) {
      res.json(error);
    }
  } catch (error) {
    res.json(error);
  }
});

// routes for editing the data
router.put("/:id/update", auth.verify, async (req, res) => {
  const id = req.params.id;
  const data = req.body;
  await InsuranceInfo.update(
    {
      InsuranceName: data.InsuranceName,
      PolicyNo: data.PolicyNo,
      GroupNo: data.GroupNo,
      GroupNo: data.GroupNo,
      Telephone: data.Telephone,
      ExpirationDate: data.ExpirationDate,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching all the data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const data = await InsuranceInfo.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(data);
});

// routes for deleting incident report data
router.put("/:id/delete", async (req, res) => {
  const deleteData = await InsuranceInfo.destroy({
    where: { id: req.params.id },
  });

  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
