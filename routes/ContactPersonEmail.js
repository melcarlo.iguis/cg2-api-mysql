const express = require("express");
const router = express.Router();
const { ContactPersonEmail } = require("../models");
const auth = require("../auth");

// routes for adding Contact Person data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;

  let contactPersonData = {
    email: data.email,
    ContactPersonId: data.ContactPersonId,
  };
  const addData = ContactPersonEmail.create(contactPersonData)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

router.put("/:contactPersonEmailID/delete", async (req, res) => {
  const deleteContactPersonEmail = await ContactPersonEmail.destroy({
    where: { id: req.params.contactPersonEmailID },
  });

  if (deleteContactPersonEmail) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for editing incident reports data
router.put("/:contactPersonEmailID/update", auth.verify, async (req, res) => {
  const data = req.body;

  await ContactPersonEmail.update(
    {
      email: data.email,
    },
    {
      where: {
        id: req.params.contactPersonEmailID,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
