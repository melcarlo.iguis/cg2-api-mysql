const express = require("express");
const router = express.Router();
const { Behaviors } = require("../models");
const auth = require("../auth");

// routes for adding behavior data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = await Behaviors.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing behavior data
router.put("/:behaviorId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Behaviors.update(
    {
      behavior: data.behavior,
      startDate: data.startDate,
      endDate: data.endDate,
      location: data.location,
      alterable: data.alterable,
      riskTo: data.riskTo,
      times: data.times,
      triggerBy: data.triggerBy,
      review: data.review,
    },
    {
      where: {
        id: req.params.behaviorId,
      },
    }
  )
    .then((result) => {
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for deleting behavior data
router.put("/:behaviorId/delete", async (req, res) => {
  const deleteBehavior = await Behaviors.destroy({
    where: { id: req.params.behaviorId },
  });

  if (deleteBehavior) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for fetching all allergy data of specific resident
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const behaviors = await Behaviors.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(behaviors);
});

module.exports = router;
