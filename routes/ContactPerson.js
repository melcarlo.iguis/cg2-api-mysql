const express = require("express");
const router = express.Router();
const { ContactPerson } = require("../models");
const auth = require("../auth");

// routes for adding Contact Person data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;

  let contactPersonData = {
    firstName: data.firstName,
    middleName: data.middleName,
    lastName: data.lastName,
    relationship: data.relationship,
    // email: data.email,
    // contact: data.contact,
    responsibility: data.responsibility,
    ResidentId: data.ResidentId,
  };
  const addData = ContactPerson.create(contactPersonData)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching the residents contact persons' info
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const contactPersonDetails = await ContactPerson.findOne({
    where: { ResidentId: residentsId },
  });
  res.json(contactPersonDetails);
});

// routes for deleting incident report data
router.put("/:contactPersonID/delete", async (req, res) => {
  const deleteContactPerson = await ContactPerson.destroy({
    where: { id: req.params.contactPersonID },
  });

  if (deleteContactPerson) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for editing incident reports data
router.put("/:personContactID/update", auth.verify, async (req, res) => {
  const data = req.body;

  await ContactPerson.update(
    {
      firstName: data.firstName,
      middleName: data.middleName,
      lastName: data.lastName,
      relationship: data.relationship,
      // email: data.email,
      // contact: data.contact,
      responsibility: data.responsibility,
    },
    {
      where: {
        id: req.params.personContactID,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
