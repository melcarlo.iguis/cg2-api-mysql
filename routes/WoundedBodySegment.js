const express = require("express");
const router = express.Router();
const { WoundedBodySegment } = require("../models");
const auth = require("../auth");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = WoundedBodySegment.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing data
router.put("/:woundedBodySegmentId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await WoundedBodySegment.update(
    {
      woundedBodySegment: data.woundedBodySegment,
    },
    {
      where: {
        id: req.params.woundedBodySegmentId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching data
router.get("/fetch", auth.verify, async (req, res) => {
  const data = await WoundedBodySegment.findAll({});
  res.json(data);
});

// routes for deleting incident report data
router.put("/:woundedBodySegmentId/delete", async (req, res) => {
  const deleteData = await WoundedBodySegment.destroy({
    where: { id: req.params.woundedBodySegmentId },
  });

  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
