const express = require("express");
const router = express.Router();
const { Users } = require("../models");
const auth = require("../auth");
const bcrypt = require("bcryptjs");

router.get("/", async (req, res) => {
  const listOfUser = await Users.findAll();
  res.json(listOfUser);
});

router.post("/create", async (req, res) => {
  const user = req.body;

  let userData = {
    email: user.email,
    username: user.username,
    password: bcrypt.hashSync(user.password, 10),
    firstName: user.firstName,
    middleName: user.middleName,
    lastName: user.lastName,
  };

  const checkDuplicateEmail = await Users.findOne({
    where: { email: user.email },
  });
  if (checkDuplicateEmail) {
    res.json("duplicate email found!!");
  } else {
    await Users.create(userData).then((result) => {
      res.json(result);
    });
  }
});

// routes for login
router.post("/login", async (req, res) => {
  const user = req.body;
  const checkEmailIfExist = await Users.findOne({
    where: { email: user.email },
  });
  if (checkEmailIfExist == null) {
    res.json(false);
  } else {
    // check if the password is correct
    const isPasswordCorrect = bcrypt.compareSync(
      user.password,
      checkEmailIfExist.password
    );
    if (isPasswordCorrect) {
      res.json({ access: auth.createAccessToken(checkEmailIfExist) });
    } else {
      res.json(isPasswordCorrect);
    }
  }
});

// get user's details
router.get("/details", auth.verify, async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  await Users.findByPk(userData.id).then((result) => {
    result.password = "";
    res.json(result);
  });
});

// routes for changing the user type of the users
router.put("/:userId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Users.update(
    {
      userType: data.userType,
      email: data.email,
      username: data.username,
      firstName: data.firstName,
      middleName: data.middleName,
      lastName: data.lastName,
    },
    {
      where: {
        id: req.params.userId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

module.exports = router;
