const express = require('express');
const router = express.Router();
const {PulseRate} = require('../models')
const auth = require('../auth')

// routes for adding Blood Pressure data 
router.post('/add' , auth.verify , async(req,res)=>{
    const data = req.body
    const addData = PulseRate.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })   
})

// routes for editing Blood Pressure data
router.put('/:pulseId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await PulseRate.update({
        date: data.date,
        pulseRate: data.pulseRate,
    },
    {
        where:{
            id: req.params.pulseId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for fetching incident report data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const pulseRate = await PulseRate.findAll({where: {ResidentId : residentsId}})
    res.json(pulseRate)
})


// routes for deleting incident report data
router.put('/:pulseId/delete' , async(req,res)=>{
    const deletePulse = await PulseRate.destroy({
        where: { id: req.params.pulseId },
    })

    if(deletePulse){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})
module.exports = router