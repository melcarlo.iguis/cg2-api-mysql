const express = require('express');
const router = express.Router()
const {HistoryLogs} = require('../models')
const auth = require('../auth')

// routes for adding History log data
router.post('/add' , auth.verify, async (req, res)=>{
    const data = req.body 
    const addData = HistoryLogs.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for editing diagnosis data
router.put('/:historyId/update' , auth.verify, async(req,res)=>{
    const data = req.body
    await HistoryLogs.update({
        date: data.date,
        title : data.title,
    },
    {
        where:{
            id: req.params.historyId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})


module.exports = router