const express = require("express");
const multer = require("multer");
const router = express.Router();
const { AssessmentDataDiagnosisAndAllergy } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = AssessmentDataDiagnosisAndAllergy.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      console.log(err);
      res.json(false);
    });
});

// routes for fetching all the assessments list the resident have
router.get("/fetch", auth.verify, async (req, res) => {
  const assessmentList = await AssessmentDataDiagnosisAndAllergy.findAll({});
  res.json(assessmentList);
});

// route for fetching specific diagnosis and allergy data
router.get("/:AssessmentCode/fetch", auth.verify, async (req, res) => {
  const AssessmentCode = req.params.AssessmentCode;
  const assessmentList = AssessmentDataDiagnosisAndAllergy.findAll({
    where: { AssessmentCode: AssessmentCode },
  });
  res.json(assessmentList);
});

module.exports = router;
