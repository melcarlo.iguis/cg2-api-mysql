const express = require("express");
const router = express.Router();
const { GeneralBodySegment } = require("../models");
const auth = require("../auth");

// routes for adding data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = GeneralBodySegment.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing data
router.put("/:generalBodySegmentId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await GeneralBodySegment.update(
    {
      generalBodySegment: data.generalBodySegment,
      WoundedBodySegmentId: data.WoundedBodySegmentId,
    },
    {
      where: {
        id: req.params.generalBodySegmentId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching data
router.get("/:WoundedBodySegmentId/fetch", auth.verify, async (req, res) => {
  const data = await GeneralBodySegment.findAll({
    where: { WoundedBodySegmentId: WoundedBodySegmentId },
  });
  res.json(data);
});

// routes for deleting incident report data
router.put("/:generalBodySegmentId/delete", async (req, res) => {
  const deleteData = await GeneralBodySegment.destroy({
    where: { id: req.params.generalBodySegmentId },
  });

  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
