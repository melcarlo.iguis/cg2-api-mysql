const express = require("express");
const router = express.Router();
const { EDocuments } = require("../models");
const auth = require("../auth");
const upload = require("../utils/multer");
const { cloudinary } = require("../utils/cloudinary");

router.post("/add", upload.single("image"), auth.verify, async (req, res) => {
  try {
    const result = await cloudinary.uploader.upload(req.file.path, {
      upload_preset: "documents",
    });
    try {
      const data = req.body;
      console.log(result);

      let inputData = {
        files: result.secure_url,
        name: data.name,
        category: data.category,
        ResidentId: data.ResidentId,
      };

      const addData = await EDocuments.create(inputData).then((saved) => {
        res.json(saved);
      });
    } catch (error) {
      res.json(error);
    }
  } catch (error) {
    res.json(error);
  }
});

// routes for editing diagnosis data
router.put("/:eDocumentsId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await EDocuments.update(
    {
      name: data.name,
      category: data.category,
    },
    {
      where: {
        id: req.params.eDocumentsId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching history log data of specific resident
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const history = await EDocuments.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(history);
});

router.put("/:residentsId/delete", async (req, res) => {
  const deleteData = await EDocuments.destroy({
    where: { id: req.params.residentsId },
  });
  if (deleteData) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});

// routes for fetching all history logs
router.get("/fetch", auth.verify, async (req, res) => {
  const allHistory = await EDocuments.findAll({});
  res.json(allHistory);
});

module.exports = router;
