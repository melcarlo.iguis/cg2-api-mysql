const express = require('express');
const router = express.Router();
const {Dietaries} = require('../models');
const auth = require('../auth');

// routes for adding Dietaries data
router.post('/add' , auth.verify, async(req,res)=>{
    const data = req.body
    const addData = await Dietaries.create(data).then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(false)
    })
})

// routes for editing dietary data
router.put('/:dietaryId/update' , auth.verify , async(req,res)=>{
    const data = req.body
    await Dietaries.update({
         dietaryPreference: data.dietaryPreference,
         startDate : data.startDate,
         dcDate : data.dcDate
    },
    {
        where:{
            id: req.params.dietaryId
        }
    }).then(result=>{
        console.log(result)
        res.json(data)
    }).catch(err=>{
        res.json(err)
    })
})

// routes for deleting behavior data
router.put('/:dietaryId/delete' , async(req,res)=>{
    const deleteDietary = await Dietaries.destroy({
        where: { id: req.params.dietaryId },
    })
    if(deleteDietary){
        res.json("deleted successfully")
    }else{
        res.json(false)
    }
})

// routes for fetching dietaries data
router.get('/:residentsId/fetch' , auth.verify, async (req,res)=>{
    const residentsId = req.params.residentsId;
    const dietaries = await Dietaries.findAll({where: {ResidentId : residentsId}})
    res.json(dietaries)
})

module.exports = router