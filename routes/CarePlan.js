const express = require("express");
const router = express.Router();
const { CarePlan } = require("../models");
const auth = require("../auth");

// routes for adding community data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = CarePlan.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});

// route to fetch careplan data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const carePlanList = await CarePlan.findAll({
    where: { ResidentId: residentsId },
  });
  res.json(carePlanList);
});

// routes for editing data
router.put("/:careplanId/complete", async (req, res) => {
  await CarePlan.update(
    {
      status: false,
      priorityLevel: 0,
    },
    {
      where: {
        id: req.params.careplanId,
      },
    }
  )
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(err);
    });
});
module.exports = router;
