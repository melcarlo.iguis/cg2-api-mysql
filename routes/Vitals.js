const express = require("express");
const router = express.Router();
const { Vitals } = require("../models");
const auth = require("../auth");

// routes for adding vitals data
router.post("/add", auth.verify, async (req, res) => {
  const data = req.body;
  const addData = Vitals.create(data)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json(false);
    });
});

// routes for editing vitals data
router.put("/:vitalsId/update", auth.verify, async (req, res) => {
  const data = req.body;
  await Vitals.update(
    {
      date: data.date,
      bodyTemp: data.bodyTemp,
      pulseRate: data.pulseRate,
      respirationRate: data.respirationRate,
      systolic: data.systolic,
      diastolic: data.diastolic,
    },
    {
      where: {
        id: req.params.vitalsId,
      },
    }
  )
    .then((result) => {
      console.log(result);
      res.json(data);
    })
    .catch((err) => {
      res.json(err);
    });
});

// routes for fetching incident report data
router.get("/:residentsId/fetch", auth.verify, async (req, res) => {
  const residentsId = req.params.residentsId;
  const vitals = await Vitals.findAll({ where: { ResidentId: residentsId } });
  res.json(vitals);
});

// routes for deleting incident report data
router.put("/:vitalsId/delete", async (req, res) => {
  const deleteVitals = await Vitals.destroy({
    where: { id: req.params.vitalsId },
  });

  if (deleteVitals) {
    res.json("deleted successfully");
  } else {
    res.json(false);
  }
});
module.exports = router;
